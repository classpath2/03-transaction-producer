# Kafka Commands

## Create topic
```
 bin/kafka-topics.sh --create --topic test-producer --bootstrap-server <broker-id>:9092
```
## List all the topics
`
bin/kafka-topics.sh --list --bootstrap-server <broker-id>:9092
`

## Describe topic
`
bin/kafka-topics.sh --describe --topic test-producer --bootstrap-server <broker-id>:9092
`

## Produce message
`
bin/kafka-console-producer.sh --topic test-producer --bootstrap-server <broker-id>:9092
`

## Consume message
`
bin/kafka-console-consumer.sh --topic test-producer --from-beginning --bootstrap-server <broker-id>:9092
`

## Delete topic
`
bin/kafka-topics.sh --delete --topic test-producer --bootstrap-server <broker-id>:9092
`

## Consuming messages

`
bin/kafka-console-consumer.sh --topic nse-stock-may --bootstrap-server <broker-id>:9092
`

## Lab-3 Transactions producer
1. Create two topics, 2 events and a unique transaction id for the producer instance
```java
    public final static String topicName1 = "producer-1";
    public final static String topicName2 = "producer-2";
    public final static int numEvents = 2;
    public final static String transaction_id = "Producer-Transaction-1";

```
2. Configure the Key serializer, Value Serializer and Transaction id config in Producer Config
```java
    props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, IntegerSerializer.class.getName());
    props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
    props.put(ProducerConfig.TRANSACTIONAL_ID_CONFIG, AppConfig.transaction_id);
```
3. Initialize the transaction
```java
   producer.initTransactions();
```
4. Start the producer transaction
```java
 producer.beginTransaction();
```
5. Send the messages to two topics and commit the transaction
```java
    for (int index = 1; index <= AppConfig.numEvents; index++) {
        producer.send(new ProducerRecord<>(AppConfig.topicName1, index, "Message-T1-" + index));
        producer.send(new ProducerRecord<>(AppConfig.topicName2, index, "Message-T1-" + index));
    }
    producer.commitTransaction();
```
6. Handle the exception and abort the transaction
```java
    producer.abortTransaction();
    producer.close();
    throw new RuntimeException(e);
```

7. Start a second transaction to test the negative scenarios
``` java
    producer.beginTransaction();
    try {
        for (int index = 1; index <= AppConfig.numEvents; index++) {
            producer.send(new ProducerRecord<>(AppConfig.topicName1, index, "Message-T2-" + index));
            producer.send(new ProducerRecord<>(AppConfig.topicName2, index, "Message-T2-" + index));
        }
        logger.info("Aborting Second Transaction.");
        throw new IllegalArgumentException("Aborting the transaction to test the failure case");
    } catch (Exception exception) {
        logger.error("Exception in Second Transaction. " + exception.getMessage());
        producer.abortTransaction();
    }finally {
        logger.info("Finished - Closing Kafka Producer.");
        producer.close();
    }
```

8. Test the topics
```
bin/kafka-console-consumer.sh --topic producer-1 --bootstrap-server <broker-id>:9092 --from-beginning
```
