package io.classpath.kafka.examples.config;

public class AppConfig {
    public final static String applicationID = "Producer-Transaction-Demo";
    public final static String bootstrapServers = "<broker-1>:9092,<broker-2>:9092";
    public final static String topicName1 = "producer-1";
    public final static String topicName2 = "producer-2";
    public final static int numEvents = 2;
    public final static String transaction_id = "Producer-Transaction-1";

}
