package io.classpath.kafka.examples.producer;

import io.classpath.kafka.examples.config.AppConfig;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.IntegerSerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Properties;

public class ProducerDemo {

    private static final Logger logger = LogManager.getLogger();

    public static void main(String[] args) {

        logger.info("Creating Kafka Producer...");
        Properties props = new Properties();
        props.put(ProducerConfig.CLIENT_ID_CONFIG, AppConfig.applicationID);
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, AppConfig.bootstrapServers);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, IntegerSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.put(ProducerConfig.TRANSACTIONAL_ID_CONFIG, AppConfig.transaction_id);

        KafkaProducer<Integer, String> producer = new KafkaProducer<>(props);

        producer.initTransactions();

        logger.info("Starting First Transaction...");
        producer.beginTransaction();
        try {
            for (int index = 1; index <= AppConfig.numEvents; index++) {
                producer.send(new ProducerRecord<>(AppConfig.topicName1, index, "Message-T1-" + index));
                producer.send(new ProducerRecord<>(AppConfig.topicName2, index, "Message-T1-" + index));
            }
            logger.info("Committing First Transaction.");
            producer.commitTransaction();
            logger.info("Successfully committed the transaction");
        } catch (Exception e) {
            logger.error("Exception in First Transaction. Aborting...");
            producer.abortTransaction();
            producer.close();
            throw new RuntimeException(e);
        }

        logger.info("Starting Second Transaction...");
        producer.beginTransaction();
        try {
            for (int index = 1; index <= AppConfig.numEvents; index++) {
                producer.send(new ProducerRecord<>(AppConfig.topicName1, index, "Message-T2-" + index));
                producer.send(new ProducerRecord<>(AppConfig.topicName2, index, "Message-T2-" + index));
            }
            logger.info("Aborting Second Transaction.");
            throw new IllegalArgumentException("Aborting the transaction to test the failure case");
        } catch (Exception exception) {
            logger.error("Exception in Second Transaction. " + exception.getMessage());
            producer.abortTransaction();
        }finally {
            logger.info("Finished - Closing Kafka Producer.");
            producer.close();
        }
    }
}
